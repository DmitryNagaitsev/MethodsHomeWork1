﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MethodsHomeTask
{
    class Program
    {
        private const double MaxH = Math.PI;
        private const double MaxT = 10;

        private const double DeltaH = Math.PI / 5;
        private const double DeltaT = 0.1;

        private const int HCount = (int)(MaxH / DeltaH) + 1;
        private const int TCount = (int)(MaxT / DeltaT) + 1;

        private const double D = 1 / (DeltaH * DeltaH);

        private const double A = D;
        private const double B = 2 * D + 1 / DeltaT;
        private const double C = D;


        static void Main(string[] args)
        {
            var exact = Exact();
            var @explicit = Explicit();
            var @implicit = Implicit();
            var crankNic = CrankNicolson();

            var errorExpl = GetError(exact, @explicit);
            var errorImpl = GetError(exact, @implicit);
            var errorCrNic = GetError(exact, crankNic);
            Console.WriteLine($"Явный метод: {errorExpl}");
            Console.WriteLine($"Неявный метод: {errorImpl}");
            Console.WriteLine($"Метод Кранка-Николсона: {errorCrNic}");
            Console.ReadKey();
        }

        private static double GetError(double[,] exact, double[,] @explicit)
        {
            var list = new List<double>();
            for (var t = 0; t < TCount; t++)
            {
                for (var h = 0; h < HCount; h++)
                {
                    list.Add(Math.Abs(exact[h, t] - @explicit[h, t]));
                }
            }
            return list.Max();
        }

        private static void Print(double[,] u, string method)
        {
            Console.WriteLine(method);
            for (var t = 0; t < TCount; t ++)
            {
                for (var h = 0; h < HCount; h ++)
                {
                    Console.Write($"{u[h, t]} ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public static double[,] Exact()
        {
            var u = new double[HCount, TCount];
            for (var t = 0; t < TCount; t++)
            {
                for (var h = 0; h < HCount; h++)
                {
                    u[h, t] = U(h * DeltaH, t * DeltaT);
                }
            }
            return u;
        }

        /// <summary>
        /// Явная
        /// </summary>
        public static double[,] Explicit()
        {
            var u = GetStartedValues();
            for (var t = 1; t < TCount; t ++)
            {
                for (var h = 1; h < HCount - 1; h ++)
                {
                    var g = G(h * DeltaH, (t - 1) * DeltaT);
                    u[h, t] = u[h, t - 1] + DeltaT * ((u[h + 1, t - 1] - 2 * u[h, t - 1] + u[h - 1, t - 1]) / (DeltaH * DeltaH) +
                                                 g);
                }
            }
            return u;
        }

        /// <summary>
        /// Неявная
        /// </summary>
        public static double[,] Implicit()
        {
            var uRun = GetStartedValues();
            var u = GetStartedValues();
            for (var t = 1; t < TCount; t++)
            {
                GetRun(t, uRun, u);
                for (var h = 1; h < HCount - 1; h++)
                {
                    u[h, t] = u[h, t - 1] + DeltaT * ((uRun[h + 1, t] - 2 * uRun[h, t] + uRun[h - 1, t]) / (DeltaH * DeltaH) +
                                                 G(h * DeltaH, (t - 1) * DeltaT));
                }
            }
            return u;
        }

        public static double[,] CrankNicolson()
        {
            var uRun = GetStartedValues();
            var u = GetStartedValues();
            for (var t = 1; t < TCount; t++)
            {
                GetRun(t, uRun, u);
                for (var h = 1; h < HCount - 1; h++)
                {
                    u[h, t] = u[h, t - 1] + DeltaT * ((u[h + 1, t - 1] - 2 * u[h, t - 1] + u[h - 1, t - 1]) / (2 * DeltaH * DeltaH) +
                    (uRun[h + 1, t] - 2 * uRun[h, t] + uRun[h - 1, t]) / (2 * DeltaH * DeltaH) + G(h * DeltaH, (t - 1) * DeltaT));
                }
            }
            return u;
        }

        public static double U(double h, double t)
        {
            return Math.Sin(h) + Math.Log(t*t+1, Math.E);
        }

        public static double UStart(double h)
        {
            return Math.Sin(h);
        }

        public static double UBorder(double t)
        {
            return Math.Log(t * t + 1, Math.E);
        }

        public static double G(double h, double t)
        {
            return Math.Sin(h) + (2 * t)/(t * t + 1);
        }

        private static double[,] GetStartedValues()
        {
            var u = new double[HCount, TCount];
            for (var t = 0; t < TCount; t++)
            {
                u[0, t] = UBorder(t * DeltaT);
                u[HCount - 1, t] = UBorder(t * DeltaT);
            }
            for (var h = 0; h < HCount; h++)
            {
                u[h, 0] = UStart(h * DeltaH);
            }
            return u;
        }

        public static double GetRunCoefA(double previous)
        {
            return  A / (B - C * previous);
        }

        public static double GetRunCoefB(double previousBeta, double previousAlpha, double eps)
        {
             return (C * previousBeta - eps) / (B - C * previousAlpha);
        }

        private static void GetRun(int t, double[,] uRun, double[,] u)
        {
            var alpha = new double[HCount];
            alpha[0] = 0;
            var beta = new double[HCount];
            beta[0] = UBorder(t * DeltaT);
            for (var h = 1; h < HCount; h++)
            {
                alpha[h] = GetRunCoefA(alpha[h - 1]);
                beta[h] = GetRunCoefB(beta[h - 1], alpha[h - 1], -u[h, t - 1]/DeltaT);
            }
            for (var h = HCount - 2; h > 0; h--)
            {
                uRun[h, t] = alpha[h] * uRun[h+1, t] + beta[h];
            }
        }
    }
}
